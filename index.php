<?php
session_start();

$inChat = false;
$password = "Br0wnF0x";

//Message to display when user exits
if(isset($_GET['logout'])){	
	
	$fp = fopen("log.html", 'a');
	fwrite($fp, "<div class='msgln'><i>User ". $_SESSION['name'] ." has left the chat session.</i><br></div>");
	fclose($fp);
	
	session_destroy();
	header("Location: index.php");
}

function loginForm(){
	echo'
	<!--[if IE]>
	<div id="iEError">
	<p> It\'s been detected that you\'re using internet explorer. This is bad. <br/>
	Feel free to use the following links to upgrade to a browser that displays the internet properly; <br/>
	<a href="www.google.com/chrome/"> Google Chrome </a> or <a href="http://www.mozilla.org/en-US/firefox/new/"> Mozilla Firefox </a></p>
	</div>
	<![endif]-->
	<div id="loginform">
	<form action="index.php" method="post">
		<p>Please enter your name to continue:</p>
		<label for="name">Name:</label>
		<input type="text" name="name" id="name" />
		<p>If you have an access key, enter it here:</p>
		<label for="pass">Key:</label>
		<input type="password" name="pass" id="pass" />
		<input type="submit" name="enter" id="enter" value="Enter" />
	</form>
	</div>
	';
}

if(isset($_POST['enter'])){
	if($_POST['name'] != ""){
		$_SESSION['name'] = stripslashes(htmlspecialchars($_POST['name']));
	}
	else{
		echo '<span class="error">Please type in a name</span>';
	}
}
if(isset($_POST['pass'])){
	if($_POST['pass'] == $password){
		$_SESSION['pass'] = $_POST['pass'];
	}
} else {
	$_SESSION['pass'] = "n^l&";
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>
<?php
	// Change title depending on who is using / what screen is available
	
	if(isset($_SESSION['name'])){
		if($_SESSION['pass'] == $password){
			echo 'Dice Lobby - DM';
		} else {
			echo 'Dice Lobby - Player';
		}
	} else {
		echo 'Dice Lobby - Log in';
	}
?>
</title>

<link type="text/css" rel="stylesheet" href="style.css" />
<style>
.hidden{
	display:none;
}
</style>

<script type="text/javascript" src="js/jquery.js"></script>

<script>
function init(){
	
	$('input[type="text"]').addClass("textInput");
	
	toggleElement = function(element, state){
		if(typeof(state)!==Number){
			state = 0;
		}
		switch(state){
			case 1:
				if($(element).hasClass("hidden")){
					$(element).removeClass("hidden");
				} 
				break;
			case 2:
				if(!$(element).hasClass("hidden")){
					$(element).addClass("hidden");
				} 
				break;
			default:
				if($(element).hasClass("hidden")){
					$(element).removeClass("hidden");
				} else {
					$(element).addClass("hidden");
				}
				break;
		}
	}
	
	getLogs = function(){
		$.post("admin.php", {command: "gRl"}, function(data){$('#restoreDiv').html(data);});
	}
	getLogs();
	
	$("#adminClear").click(function(){
		$.post("admin.php", {command: "cCc"});
	});
	$("#adminBackup").click(function(){
		$.post("admin.php", {command: "bBb"});
		getLogs();
	});	
	$("#adminRestore").click(function(){
		toggleElement("#restoreDiv");
	});	
	$("body").on("click",".restoreEntry", function(){
		logfile = $(this).attr('id');
		$.post("admin.php", {command: "rRr", restore: logfile});
		toggleElement("#restoreDiv", 2);
	});
	$("#showButton").click(function(){
		if($("#charList").hasClass("hidden")){
			$.post("characterParser.php", {command: "gCl"}, function(data){$('#charList').html(data);});
		}
		toggleElement("#charList");
		toggleElement("#charStats", 2);
	});
	
	$("body").on("click", "#newChar", function(){
		toggleElement("#charStats", 1);
		$.ajax({
			url: "newCharForm.html",
			cache: false,
			success: function(html){		
				$("#charStats").html(html);
		  	},
		});
	});
}
</script>
</head>
<body onload="init()">
<?php
if(!isset($_SESSION['name'])){
	loginForm();
}
else{
?>
<!--[if IE]>
	<div id="iEError">
	<p> It's been detected that you're using internet explorer. This is bad. <br/>
	Feel free to use the following links to upgrade to a browser that displays the internet properly;</br>
	<a href="www.google.com/chrome/"> Google Chrome </a> or <a href="http://www.mozilla.org/en-US/firefox/new/"> Mozilla Firefox </a></p>
	</div>
<![endif]-->
<div id="wrapper">
	<div id="menu">
		<p class="welcome">Welcome, <b><?php echo $_SESSION['name']; ?></b></p>
        <p class="menuLink"><a id="back" href="#">Back to list</a></p>
		<p class="menuLink"><a id="exit" href="#">Exit Chat</a></p>
		<div style="clear:both"></div>
	</div>	
	<div id="chatbox"><?php
	if(file_exists("log.html") && filesize("log.html") > 0){
		$handle = fopen("log.html", "r");
		$contents = fread($handle, filesize("log.html"));
		fclose($handle);
		
		echo $contents;
	}
	?></div>
	
	<form name="message" action="">
		<input name="usermsg" type="text" id="usermsg" size="63" />
		<input name="submitmsg" type="submit"  id="submitmsg" value="Send" />
	</form>
	<form name="dice" action="">
		<label>Roll a D</label>
		<input name="sides" type="text" id="diceSides" size="4" />
		<label> this many times;</label>
		<input name="amount" type="text" id="diceAmount" size="4" />
		<input name="diceRoll" type="button" id="diceRoll" value="Roll" />
	</form>
	<form name="DM" action=""<?php if(!($_SESSION["pass"] == $password)){echo 'class="hidden"';} ?> >
		<label>Roll a D</label>
		<input name="gaffSides" type="text" id="gaffSides" size="4" />
		<label> this many times;</label>
		<input name="gaffAmount" type="text" id="gaffAmount" size="4" />
		<label> and get this result;</label>
		<input name="gaffNum" type="text" id="gaffNum" size="4" />
		<input name="gaffRoll" type="button" id="gaffRoll" value="Roll" />
	</form>
<?php if($_SESSION["pass"] == $password){
	echo '
		<form name="admin" action="">
			<input type="button" id="adminClear" name="adminClear" value="Clear Chat" />
			<input type="button" id="adminBackup" name="adminBackup" value="Backup Chat" />
			<input type="button" id="adminRestore" name="adminRestore" value="Restore Backup" />
		</form>
		
		<div class="hidden" id="restoreDiv"></div>';
} ?> 
</div>

<div id="charSheets">
	<div id="showButton"> ^ View Character Info ^ </div>
	<div id="charList" class='hidden'></div>
	<div id="charStats" class='hidden'></div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#submitmsg").click(function(){ 
		var clientmsg = $("#usermsg").val();
		$.post("post.php", {text: clientmsg}); 
		$("#usermsg").attr("value", "");
		return false;
	});
	
	$("#diceRoll").click(function(){
		if(!($("#diceSides").val()).match(/^\d+$/)){
			alert("A dice can't have " + $("#diceSides").val() + " sides, it needs to be a number");
		} else {
			if(!($("#diceAmount").val()).match(/^\d+$/)){
				alert("You can't have " + $("#diceAmount").val() + " dice, it should be a number");
			} else {
				var numOfSides=parseInt($("#diceSides").val());
				var numOfDice = parseInt($("#diceAmount").val());
				
				var result = 0;
				for(var i = 0; i<numOfDice; i++){
					result = result + Math.floor(Math.random()*numOfSides) + 1;
				}
				var msg = "rolled a D" + numOfSides + " " + numOfDice + " times, the result was " + result + ".";
				$.post("post.php", {text: msg, textType: "Bold"});
				$("#diceSides").attr("value", "");
				$("#diceAmount").attr("value", "");
			}
		}
	});

	$("#gaffRoll").click(function(){
		if(!($("#gaffSides").val()).match(/^\d+$/)){
			alert("A dice can't have " + $("#gaffSides").val() + " sides, it needs to be a number");
		} else {
			if(!($("#gaffAmount").val()).match(/^\d+$/)){
				alert("You can't have " + $("#gaffAmount").val() + " dice, it should be a number");
			} else {
				if(!($("#gaffNum").val()).match(/^\d+$/)){
				alert("You can't roll " + $("#gaffNum").val() + " on any dice, it isn't a number");
				} else {
					var numOfSides = parseInt($("#gaffSides").val());
					var numOfDice = parseInt($("#gaffAmount").val());
					var result = parseInt($("#gaffNum").val());
					
					var msg = "rolled a D" + numOfSides + " " + numOfDice + " times, the result was " + result + ".";
					$.post("post.php", {text: msg, textType: "Bold"});
					$("#gaffSides").attr("value", "");
					$("#gaffAmount").attr("value", "");
					$("#gaffNum").attr("value", "");
				}
			}
		}
	});

	//Load the file containing the chat log
	function loadLog(){		
		var oldscrollHeight = $("#chatbox").attr("scrollHeight") - 20;
		$.ajax({
			url: "log.html",
			cache: false,
			success: function(html){		
				$("#chatbox").html(html); //Insert chat log into the #chatbox div				
				var newscrollHeight = $("#chatbox").attr("scrollHeight") - 20;
				if(newscrollHeight > oldscrollHeight){
					$("#chatbox").animate({ scrollTop: newscrollHeight }, 'normal'); //Autoscroll to bottom of div
				}				
		  	},
		});
	}
	setInterval (loadLog, 1500);	//Reload file every 1.5 seconds
	
	//If user wants to end session
	$("#exit").click(function(){
		var exit = confirm("Are you sure you want to end the session?");
		if(exit==true){window.location = 'index.php?logout=true';}		
	});
});
</script>
<?php
}
?>
</body>
</html>